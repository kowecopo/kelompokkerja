 <!-- CoreUI CSS -->
 <link rel="stylesheet" href="/css/app.css" crossorigin="anonymous">

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />


 <link rel=" stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css " />
 <link rel=" stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css " />
 <link rel=" stylesheet" type="text/css" href="https://cdn.datatables.net/rowgroup/1.1.3/css/rowGroup.dataTables.min.css" />


 <x-app-layout>


     <div class="py-12">
         <div>
             <div class="col-md-6" style="left: 90px;">
                 <a type=" button" href="{{route('view_create')}}" class="btn btn-primary col-md-2">Create</a>
             </div>
             <br>
             <div>
                 <div class="card" style="width: 100rem; margin: 0 auto; ">
                     <div class="card-header"><strong>Master Pegawai</strong></div>
                     <div class="card-body">
                         <table id="example" class="table table-striped table-bordered" style="width:100%">
                             <thead>
                                 <tr>
                                     <td>Nama Pegawai</td>
                                     <td>Jabatan</td>
                                     <td width="10%">Action</td>
                                 </tr>
                             </thead>
                             <tbody>
                                 @foreach ($pegawai as $pegawais)
                                 <tr>
                                     <td>{{$pegawais->nama_pegawai}}</td>
                                     <td>{{$pegawais->jabatan_pegawai}}</td>
                                     <td>
                                         <a type="button" href="{{url('pegawai_edit/'.  $pegawais->id_pegawai)}}" style="margin:2px;" class="btn btn-warning btn-sm">Update</a>
                                         <br>
                                         <a type="button" href="{{url('pegawai_delete/'.  $pegawais->id_pegawai)}}" style="margin:2px;" class="btn btn-danger btn-sm">Delete</a>

                                     </td>
                                 </tr>
                                 @endforeach
                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     </div>
 </x-app-layout>