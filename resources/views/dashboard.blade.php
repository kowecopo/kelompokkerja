   <!-- CoreUI CSS -->
   <link rel="stylesheet" href="/css/app.css" crossorigin="anonymous">

   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />


   <link rel=" stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css " />
   <link rel=" stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css " />
   <link rel=" stylesheet" type="text/css" href="https://cdn.datatables.net/rowgroup/1.1.3/css/rowGroup.dataTables.min.css" />

   <x-app-layout>

       <div class="py-12">
           <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
               <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                   <div class="card">
                       <div class="card-header"><strong>Jumlah Pekerja</strong></div>
                       <div class="card-body row center">
                           <div class="col-12 col-sm-6 col-md-4">
                               <div class="info-box mb-3">
                                   <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-user-tie"></i></span>
                                   <div class="info-box-content">
                                       <span class="info-box-text">Jumlah Penguji</span>
                                       <span class="info-box-number">{{$penguji}}</span>
                                   </div>
                               </div>
                           </div>
                           <div class="col-12 col-sm-6 col-md-4">
                               <div class="info-box mb-3">
                                   <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-user"></i></span>
                                   <div class="info-box-content">
                                       <span class="info-box-text">Jumlah Staff</span>
                                       <span class="info-box-number">{{$pegawai}}</span>
                                   </div>
                               </div>
                           </div>

                           <div class="col-12 col-sm-6 col-md-4">
                               <div class="info-box mb-3">
                                   <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
                                   <div class="info-box-content">
                                       <span class="info-box-text">Total Pegawai</span>
                                       <span class="info-box-number">{{$total}}</span>
                                   </div>
                                   <!-- /.info-box-content -->
                               </div>
                               <!-- /.info-box -->
                           </div>

                       </div>


                   </div>
               </div>
               <br>

               <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                   <div class="card">
                       <div class="card-header"><strong>Data Random Terakhir</strong></div>
                       <div class="card-body">
                           <table id="example" class="table table-striped table-bordered" style="width:100%">
                               <thead>
                                   <tr>
                                       <td width="10%">Minggu/Kelompok</td>
                                       <td>Kelompok 1</td>
                                       <td>Kelompok 2</td>
                                       <td>Kelompok 3</td>
                                       <td>Kelompok 4</td>
                                   </tr>
                               </thead>
                               <tbody>
                                   @foreach([1, 2, 3, 4] as $minggu)
                                   <tr>
                                       <td> Minggu Ke {{$minggu}}</td>
                                       @foreach(['a', 'b', 'c', 'd'] as $group)
                                       <td>
                                           @if(isset($kelompoks))
                                           @foreach($kelompoks[$minggu.$group] as $kelompok )
                                           {{$kelompok }} </br>
                                           @endforeach
                                           @endif

                                       </td>
                                       @endforeach
                                   </tr>
                                   @endforeach
                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>



           </div>
       </div>

   </x-app-layout>