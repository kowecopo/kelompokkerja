   <!-- CoreUI CSS -->
   <link rel="stylesheet" href="/css/app.css" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />


<link rel=" stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css " />
<link rel=" stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css " />
<link rel=" stylesheet" type="text/css" href="https://cdn.datatables.net/rowgroup/1.1.3/css/rowGroup.dataTables.min.css" />


<x-app-layout>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
        <form action="{{url('pegawai_create_lesgo') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
				<div class="container-fluid">
					<div class="form-group">
						<label>Nama Lengkap</label>
						<input class="form-control"  name="nama_pegawai" type="text" required >
					</div>
					<div class="form-group">
						<label>Jabatan Pegawai</label>
						<br>
						<select name="jabatan_pegawai" id="jabatan_pegawai">
							<option value="">Pilih Jabatan</option>
							<option value="PENGUJI">Penguji</option>
							<option value="STAFF">Staff</option>

					</select>
					</div>
					<div class="form-group">
						<label>Jenis Pegawai</label>
						<br>

					<select name="jenis_pegawai" id="jenis_pegawai">
							<option value="">Pilih Jenis Pegawai</option>
							<option value="1">Penguji</option>
							<option value="2">Staff</option>
							<option value="3">Admin</option>
					</select>
					</div>
				</div>
				<br>
                <div class="col-md-2">
                        <button class="btn btn-block btn-warning" type="submit">Simpan</button>
                    </div>
        </form>

			</div>
            </div>
        </div>
    </div>
</x-app-layout>
