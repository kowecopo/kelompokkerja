 <!-- CoreUI CSS -->
 <link rel="stylesheet" href="/css/app.css" crossorigin="anonymous">

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />


 <link rel=" stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css " />
 <link rel=" stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css " />
 <link rel=" stylesheet" type="text/css" href="https://cdn.datatables.net/rowgroup/1.1.3/css/rowGroup.dataTables.min.css" />


 <x-app-layout>


     <div class="py-12">
         <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
             <div class="form-row">
                 <div class="col-md-6">
                     <select name="bulan" id="bulan">
                         <option value="">Pilih Bulan</option>
                         <option value="1">January</option>
                         <option value="2">Februari</option>
                         <option value="3">Maret</option>
                         <option value="4">April</option>
                         <option value="5">Mei</option>
                         <option value="6">Juni</option>
                         <option value="7">Juli</option>
                         <option value="8">Agustus</option>
                         <option value="9">September</option>
                         <option value="10">Oktober</option>
                         <option value="11">November</option>
                         <option value="12">Desember</option>
                     </select>
                 </div>
                 <div class="col-md-5">
                     <a type="button" href="{{route('truncate_view')}}" style="float: right;" class="btn btn-danger">Reset Data</a>
                 </div>
             </div>
             <br>
             <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                 <div class="card">
                     <div class="card-header"><strong>Data Berdasarkan Bulan</strong></div>
                     <div class="card-body">
                         <table id="example" class="table table-striped table-bordered" style="width:100%">
                             <thead>
                                 <tr>
                                     <td>Minggu/Kelompok Pegawai</td>
                                     <td>Kelompok 1</td>
                                     <td>Kelompok 2</td>
                                     <td>Kelompok 3</td>
                                     <td>Kelompok 4</td>
                                 </tr>
                             </thead>
                             <tbody>

                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     </div>
 </x-app-layout>