   <!-- CoreUI CSS -->
   <link rel="stylesheet" href="/css/app.css" crossorigin="anonymous">

   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />


   <link rel=" stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css " />
   <link rel=" stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css " />
   <link rel=" stylesheet" type="text/css" href="https://cdn.datatables.net/rowgroup/1.1.3/css/rowGroup.dataTables.min.css" />
   <x-app-layout>

       <div class="py-12">
           <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
               <div class="form-row">
                   <div class="col-md-11">
                       <a type="button" href="{{route('random_pegawai')}}" class="btn btn-primary col-md-2">Random</a>
                   </div>
                   <div class="col-md-1">
                       <a type="button" href="{{route('simpan_pegawai')}}" class="btn btn-success">Simpan</a>
                   </div>
               </div>
               <br>
               <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                   <div class="card">
                       <div class="card-header"><strong>Data Random </strong></div>
                       <div class="card-body">
                           <table id="example" class="table table-striped table-bordered" style="width:100%">
                               <thead>
                                   <tr>
                                       <td width="10%">Minggu/Kelompok</td>
                                       <td>Kelompok 1</td>
                                       <td>Kelompok 2</td>
                                       <td>Kelompok 3</td>
                                       <td>Kelompok 4</td>
                                   </tr>
                               </thead>
                               <tbody>

                                   @foreach([1, 2, 3, 4] as $minggu)
                                   <tr>
                                       <td>Minggu Ke {{$minggu}}</td>
                                       @foreach(['a', 'b', 'c', 'd'] as $group)
                                       <td>
                                           @if(isset($kelompoks))
                                           @foreach($kelompoks[$minggu.$group] as $kelompok)
                                           {{$kelompok}} </br>
                                           @endforeach
                                           @endif
                                       </td>
                                       @endforeach
                                   </tr>
                                   @endforeach



                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
               <br>
               <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                   <div class="card">
                       <div class="card-header"><strong>Data Random </strong></div>
                       <div class="card-body">
                           <table id="example" class="table table-striped table-bordered" style="width:100%">
                               <thead>
                                   <tr>
                                       <td width="360px">Nama Pegawai</td>
                                       <td width="50px">Minggu ke 1</td>
                                       <td width="50px">Minggu ke 2</td>
                                       <td width="50px">Minggu ke 3</td>
                                       <td width="50px">Minggu ke 4 </td>
                                       <td width="10px">Action</td>
                                   </tr>
                               </thead>
                               <tbody>
                                   @foreach ($pegawai as $pegawais)
                                   <tr>
                                       <td>{{$pegawais->nama_pegawai}}</td>
                                       <td>{{$pegawais->flag_1}}</td>
                                       <td>{{$pegawais->flag_2}}</td>
                                       <td>{{$pegawais->flag_3}}</td>
                                       <td>{{$pegawais->flag_4}}</td>
                                       <td>
                                           <a type="button" href="{{url('random_edit/'. $pegawais->id_pegawai)}}" class="btn btn-warning">Update</a>


                                       </td>
                                   </tr>
                                   @endforeach
                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
               <br>

           </div>
       </div>
       </div>
       <br>

   </x-app-layout>