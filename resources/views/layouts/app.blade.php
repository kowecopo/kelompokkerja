<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Kelompok Kerja') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <link rel="stylesheet" href="https://adminlte.io/themes/v3/dist/css/adminlte.min.css">

    @livewireStyles

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>



</head>
<style>
    .navbar_a {
        position: fixed;
        width: 100%;
        z-index: 999;
    }

    .main {
        padding: 50px;
        height: 100px;
        /* Used in this example to enable scrolling */
    }

    .center {
        margin: -10;
        transform: translate(0%, 5%);
    }
</style>

<body class="font-sans antialiased">
    <x-jet-banner />

    <div class="min-h-screen bg-gray-1000 ">
        <div class="navbar_a">
            @livewire('navigation-menu')
            <!-- Page Heading -->
            @if (isset($header))
            <header class="bg-white shadow ">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>
            @endif
        </div>


        <!-- Page Content -->
        <main class="main">
            {{ $slot }}
        </main>
    </div>

    @stack('modals')

    @livewireScripts
    <script>
        $('select[name="bulan"]').change(function() {
            $.ajax({
                method: 'get',
                url: `{{ route('show_data') }}?id=` + $('select[name="bulan"]').val(),
                dataType: 'json',
                success: function(response) {
                    if (response.code = 1) {
                        html = '';
                        $.each([1, 2, 3, 4], function(index, minggu) {
                            html += '<tr>';
                            html += '<td>' + 'Minggu ke ' + minggu + '</td>';
                            $.each(['a', 'b', 'c', 'd'], function(index1, group) {
                                html += '<td>';
                                $.each(response.kelompoks[minggu + group], function(index2, kelompok) {
                                    html += kelompok + '</br>';
                                });
                                html += '</td>';
                            });
                            html += '</tr>';
                        });
                        $('tbody').html(html);


                        // $.each(response.pegawai, function(index, pegawai) {
                        //     html += '<tr class="font-weight-bold">';
                        //     html += '<td>'+pegawai.nama_pegawai+'</td>';
                        //     html += '<td>'+pegawai.flag_1+'</td>';
                        //     html += '<td>'+pegawai.flag_2+'</td>';
                        //     html += '<td>'+pegawai.flag_3+'</td>';
                        //     html += '<td>'+pegawai.flag_4+'</td>';

                        //     html += '</tr>';
                        // });


                    }
                }
            });
        });
    </script>
</body>

</html>