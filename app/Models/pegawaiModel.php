<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class pegawaiModel extends Model
{
    public $table = "pegawai";


	public function create_pegawai($data)
	{
		return DB::table('pegawai')->insert($data);
	}

	public function deleteData($id)
	{
		return DB::table('pegawai')
		->where('id_pegawai', $id)
		->delete();
	}

	
	public function update_pegawai($id, $data)
	{
		return DB::table('pegawai')
		->where('id_pegawai', $id)
		->update($data);
	}

	public function simpan_lain(){
	DB::statement('INSERT INTO view_data (`nama_pegawai`, `jenis_pegawai`, `jabatan_pegawai`, `flag_1`, `flag_2`, `flag_3`, `flag_4`, `created_at`)
	SELECT `nama_pegawai`, `jenis_pegawai`, `jabatan_pegawai`, `flag_1`, `flag_2`, `flag_3`, `flag_4`, `created_at` FROM pegawai');
	}

	public function truncate(){
		DB::statement('TRUNCATE TABLE view_data');}

	public function ReadData()
	{
		return DB::table('pegawai')->get();
	}

	public function ReadDataBaru()
	{
		return DB::table('view_data')->orderBy('created_at', 'desc')->limit(26)->get();
	}

	public function Flag1(){
		return DB::table('pegawai')->orderBy('created_at', 'desc')->limit(26)->orderBy('flag_1', 'desc')->get();
	}
	public function Flag2(){
		return DB::table('pegawai')->orderBy('created_at', 'desc')->limit(26)->orderBy('flag_2', 'desc')->get();
	}
	public function Flag3(){
		return DB::table('pegawai')->orderBy('created_at', 'desc')->limit(26)->orderBy('flag_3', 'desc')->get();
	}
	public function Flag4(){
		return DB::table('pegawai')->orderBy('created_at', 'desc')->limit(26)->orderBy('flag_4', 'desc')->get();
	}

	public function detailEdit($id)
    {
        return DB::table('pegawai')->where('id_pegawai', $id)->first();
    }

    public function update_random($id, $data)
    {
        return DB::table('pegawai')
            ->where('id_pegawai', $id)
            ->update($data);
    }

	public function Data3()
	{
		return DB::table('pegawai')
		->where([
			['flag_3',  0],
			['flag_2', 0],
			['flag_1', 0]
		])
		->orWhere(function($query) {
			$query->where('flag_3', 0)
				  ->whereNotNull('flag_2')
				  ->where('flag_1', 0);
		})
		->orWhere(function($query) {
			$query->where('flag_3', 0)
				  ->where('flag_2', 0)
				  ->whereNotNull('flag_1');
		})
		->get();
	}

	public function Data4()
	{
		return DB::table('pegawai')
		->where([
			['flag_4',  0],
			['flag_3', 0],
			['flag_2', 0]
		])
		->orWhere(function($query) {
			$query->where('flag_4', 0)
				  ->whereNotNull('flag_3')
				  ->where('flag_2', 0);
		})
		->orWhere(function($query) {
			$query->where('flag_4', 0)
				  ->where('flag_3', 0)
				  ->whereNotNull('flag_2');
		})
		->get();
	}


	public function countDatatotal(){
		return DB::table('pegawai')->count();
	}
	

	public function countDataPegawai(){
		return DB::table('pegawai')->where('jenis_pegawai', 2)->orWhere('jenis_pegawai', 3)->count();
	}
	
	public function countDataPenguji(){
		return DB::table('pegawai')->where('jenis_pegawai', 1)->count();
	}

	public function updatebatch1($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_1' => '1a']);
	}

	public function updatebatch2($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_1' => '1b']);
	}

	public function updatebatch3($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_1' => '1c']);
	}

	public function updatebatch4($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_1' => '1d']);
	}
	public function updatebatch5($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_2' => '2a']);
	}
	public function updatebatch6($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_2' => '2b']);
	}
	public function updatebatch7($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_2' => '2c']);
	}
	public function updatebatch8($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_2' => '2d']);
	}
	public function updatebatch9($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_3' => '3a']);
	}
	public function updatebatch10($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_3' => '3b']);
	}
	public function updatebatch11($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_3' => '3c']);
	}
	public function updatebatch12($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_3' => '3d']);
	}
	public function updatebatch13($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_4' => '4a']);
	}
	public function updatebatch14($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_4' => '4b']);
	}
	public function updatebatch15($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_4' => '4c']);
	}
	public function updatebatch16($group)
	{
		return DB::table('pegawai')->whereIn('id_pegawai', $group)->update(['flag_4' => '4d']);
	}

	



}
