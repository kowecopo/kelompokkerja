<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pegawaiModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pegawai extends Controller
{

    public function __construct()
	{
		$this->pegawaiModel = new pegawaiModel();
	}

	
	public function deleteData($id)
	{
		$this->pegawaiModel->deleteData($id);
		return redirect()->route('pegawai')->with('pesan', 'Data berhasil Di delete');
	}

	public function index(){
		$data = [
			'pegawai' => $this->pegawaiModel->ReadData(),
		];
		return view('pegawai', $data);
	}

	public function view(){
		$data = [
			'pegawai' => $this->pegawaiModel->ReadData(),
		];
		return view('view_data', $data);
	}

	public function view_create(){

		return view('create_pegawai');

	}

	public function create_pegawai(){

		$pegawai = [
			'nama_pegawai' => Request()->nama_pegawai,
			'jenis_pegawai' => Request()->jenis_pegawai,
			'jabatan_pegawai' => Request()->jabatan_pegawai,
			'flag_1' => 0,
			'flag_2' => 0,
			'flag_3' => 0,
			'flag_4' => 0,
		];
		$this->pegawaiModel->create_pegawai($pegawai);
		return redirect()->route('pegawai')->with('success', 'Data berhasil diganti');
	}

	public function view_edit($id){

		$data = [
			'pegawai' => $this->pegawaiModel->detailedit($id),
		];
		return view('edit_pegawai', $data);

	}

	public function update_pegawai($id){

		$pegawai = [
			'nama_pegawai' => Request()->nama_pegawai,
			'jenis_pegawai' => Request()->jenis_pegawai,
			'jabatan_pegawai' => Request()->jabatan_pegawai,
		];

		$this->pegawaiModel->update_pegawai($id, $pegawai);
		return redirect()->route('pegawai')->with('success', 'Data berhasil diganti');
	}

}


//kodingan lama

//{
				// // kelompok 1
			// $collection = collect([1, 2, 3]);
			// $random = json_decode($collection->random(1));
			// $user = DB::table('pegawai')
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })
			// ->where('jenis_pegawai', $random[0])
			// ->inRandomOrder()->limit(2)
			// ->pluck('id_pegawai');
			// do {
			// $random1 = json_decode($collection->random(1));
			// } while ($random1[0] == $random[0]);
			// $user1 = DB::table('pegawai')
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })
			// ->where('jenis_pegawai', $random1[0])

			// ->inRandomOrder()->limit(1)
			// ->pluck('id_pegawai');
			// do {
			// $random2 = json_decode($collection->random(1));
			// } while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
			// $user2 = DB::table('pegawai')
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })
			// ->where('jenis_pegawai', $random2[0])

			// ->inRandomOrder()->limit(1)
			// ->pluck('id_pegawai');
			// $users = json_decode($user);
			// $users1 = json_decode($user1);
			// $users2 = json_decode($user2);
			// $grup = array_merge($users, $users1, $users2);
			// dd($grup);exit;

			// $this->pegawaiModel->updatebatch9($grup);


			// 			// kelompok 2
			// 			$collection = collect([1, 2, 3]);
			// 			$random = json_decode($collection->random(1));
			// 			$user = DB::table('pegawai')
			// 			->where([
			// 				['flag_3',  0],
			// 				['flag_2', 0],
			// 				['flag_1', 0]
			// 			])
			// 			->orWhere(function($query) {
			// 				$query->where('flag_3', 0)
			// 					  ->whereNotNull('flag_2')
			// 					  ->where('flag_1', 0);
			// 			})
			// 			->orWhere(function($query) {
			// 				$query->where('flag_3', 0)
			// 					  ->where('flag_2', 0)
			// 					  ->whereNotNull('flag_1');
			// 			})
			// 			->where('jenis_pegawai', $random[0])
			// 			->inRandomOrder()->limit(2)
			// 			->pluck('id_pegawai');

			// 			dd($user);exit;
			// 			do {
			// 			$random1 = json_decode($collection->random(1));
			// 			} while ($random1[0] == $random[0]);
			// 			$user1 = DB::table('pegawai')
			// 			->where([
			// 				['flag_3',  0],
			// 				['flag_2', 0],
			// 				['flag_1', 0]
			// 			])
			// 			->orWhere(function($query) {
			// 				$query->where('flag_3', 0)
			// 					  ->whereNotNull('flag_2')
			// 					  ->where('flag_1', 0);
			// 			})
			// 			->orWhere(function($query) {
			// 				$query->where('flag_3', 0)
			// 					  ->where('flag_2', 0)
			// 					  ->whereNotNull('flag_1');
			// 			})
			// 			->where('jenis_pegawai', $random1[0])
			
			// 			->inRandomOrder()->limit(1)
			// 			->pluck('id_pegawai');
			// 			do {
			// 			$random2 = json_decode($collection->random(1));
			// 			} while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
			// 			$user2 = DB::table('pegawai')
			// 			->where([
			// 				['flag_3',  0],
			// 				['flag_2', 0],
			// 				['flag_1', 0]
			// 			])
			// 			->orWhere(function($query) {
			// 				$query->where('flag_3', 0)
			// 					  ->whereNotNull('flag_2')
			// 					  ->where('flag_1', 0);
			// 			})
			// 			->orWhere(function($query) {
			// 				$query->where('flag_3', 0)
			// 					  ->where('flag_2', 0)
			// 					  ->whereNotNull('flag_1');
			// 			})
			// 			->where('jenis_pegawai', $random2[0])
			
			// 			->inRandomOrder()->limit(1)
			// 			->pluck('id_pegawai');
			// 			$users = json_decode($user);
			// 			$users1 = json_decode($user1);
			// 			$users2 = json_decode($user2);
			// 			$grup = array_merge($users, $users1, $users2);
			// 			$this->pegawaiModel->updatebatch10($grup);

			// kelompok 2
			// $collection = collect([1, 2, 3]);
			// $random = json_decode($collection->random(1));
			// $user = DB::table('pegawai')
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })
			// ->where('jenis_pegawai', $random[0])

			// ->inRandomOrder()->limit(2)
			// ->pluck('id_pegawai');
			// do {
			// $random1 = json_decode($collection->random(1));
			// } while ($random1[0] == $random[0]);
			// $user1 = DB::table('pegawai')
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })
			// ->where('jenis_pegawai', $random1[0])


			// ->inRandomOrder()->limit(1)
			// ->pluck('id_pegawai');
			// do {
			// $random2 = json_decode($collection->random(1));
			// } while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
			// $user2 = DB::table('pegawai')
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })
			// ->where('jenis_pegawai', $random2[0])

			// ->inRandomOrder()->limit(1)
			// ->pluck('id_pegawai');
			// $users = json_decode($user);
			// $users1 = json_decode($user1);
			// $users2 = json_decode($user2);
			// $grup = array_merge($users, $users1, $users2);
			// $this->pegawaiModel->updatebatch10($grup);


			// // kelompok 3
			// $collection = collect([1, 2, 3]);
			// $random = json_decode($collection->random(1));
			// $user = DB::table('pegawai')
			// ->where('jenis_pegawai', $random[0])
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })
			// ->inRandomOrder()->limit(2)
			// ->pluck('id_pegawai');
			// do {
			// $random1 = json_decode($collection->random(1));
			// } while ($random1[0] == $random[0]);
			// $user1 = DB::table('pegawai')
			// ->where('jenis_pegawai', $random1[0])
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })

			// ->inRandomOrder()->limit(1)
			// ->pluck('id_pegawai');
			// do {
			// $random2 = json_decode($collection->random(1));
			// } while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
			// $user2 = DB::table('pegawai')
			// ->where('jenis_pegawai', $random2[0])
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })
			// ->inRandomOrder()->limit(1)
			// ->pluck('id_pegawai');
			// $users = json_decode($user);
			// $users1 = json_decode($user1);
			// $users2 = json_decode($user2);
			// $grup = array_merge($users, $users1, $users2);
			// $this->pegawaiModel->updatebatch11($grup);

			// // kelompok 12
			// $collection = collect([1, 2, 3]);
			// $random = json_decode($collection->random(1));
			// $user = DB::table('pegawai')
			// ->where('jenis_pegawai', $random[0])
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })

			// ->inRandomOrder()->limit(2)
			// ->pluck('id_pegawai');
			// do {
			// $random1 = json_decode($collection->random(1));
			// } while ($random1[0] == $random[0]);
			// $user1 = DB::table('pegawai')
			// ->where('jenis_pegawai', $random1[0])
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })
			// ->inRandomOrder()->limit(1)
			// ->pluck('id_pegawai');
			// do {
			// $random2 = json_decode($collection->random(1));
			// } while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
			// $user2 = DB::table('pegawai')
			// ->where('jenis_pegawai', $random2[0])
			// ->where([
			// 	['flag_3',  0],
			// 	['flag_2', 0],
			// 	['flag_1', 0]
			// ])
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->whereNotNull('flag_2')
            //           ->where('flag_1', 0);
            // })
			// ->orWhere(function($query) {
            //     $query->where('flag_3', 0)
			// 		  ->where('flag_2', 0)
            //           ->whereNotNull('flag_1');
            // })
			// ->inRandomOrder()->limit(1)
			// ->pluck('id_pegawai');
			// $users = json_decode($user);
			// $users1 = json_decode($user1);
			// $users2 = json_decode($user2);
			// $grup = array_merge($users, $users1, $users2);
			// $this->pegawaiModel->updatebatch12($grup);

		// kelompok 2
        // $collection = collect([1, 2, 3]);
		// $random = json_decode($collection->random(1));
	    // $user = DB::table('pegawai')
		// ->where('jenis_pegawai', $random[0])
		// ->orWhere(function($query) {
		// 	$query->where('flag_1', '0')
		// 		  ->whereNotNull('flag_2');
		// })
		// ->orWhere(function($query) {
		// 	$query->where('flag_2', '0')
		// 		  ->whereNotNull('flag_1');
		// })
		// ->orWhere(function($query) {
		// 	$query->where('flag_2', '0')
		// 		  ->where('flag_1', '0');
		// })
		// ->inRandomOrder()->limit(2)
		// ->pluck('id_pegawai');
		// do {
		// $random1 = json_decode($collection->random(1));
		// } while ($random1[0] == $random[0]);
		// $user1 = DB::table('pegawai')
		// ->where('jenis_pegawai', $random1[0])
		// ->orWhere(function($query) {
		// 	$query->where('flag_1', '0')
		// 		  ->whereNotNull('flag_2');
		// })
		// ->orWhere(function($query) {
		// 	$query->where('flag_2', '0')
		// 		  ->whereNotNull('flag_1');
		// })
		// ->orWhere(function($query) {
		// 	$query->where('flag_2', '0')
		// 		  ->where('flag_1', '0');
		// })
		// ->inRandomOrder()->limit(1)
		// ->pluck('id_pegawai');
		// do {
		// $random2 = json_decode($collection->random(1));
		// } while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
		// $user2 = DB::table('pegawai')
		// ->where('jenis_pegawai', $random2[0])
		// ->orWhere(function($query) {
		// 	$query->where('flag_1', '0')
		// 		  ->whereNotNull('flag_2');
		// })
		// ->orWhere(function($query) {
		// 	$query->where('flag_2', '0')
		// 		  ->whereNotNull('flag_1');
		// })
		// ->orWhere(function($query) {
		// 	$query->where('flag_2', '0')
		// 		  ->where('flag_1', '0');
		// })
		// ->inRandomOrder()->limit(1)
		// ->pluck('id_pegawai');
		// $grup = array_merge($user, $user1, $user2);
		// $this->pegawaiModel->updatebatch10($grup);


		// 		// kelompok 3
		// 		$collection = collect([1, 2, 3]);
		// 		$random = json_decode($collection->random(1));
		// 		$user = DB::table('pegawai')
		// 		->where('jenis_pegawai', $random[0])
		// 		->orWhere(function($query) {
		// 			$query->where('flag_1', '0')
		// 				  ->whereNotNull('flag_2');
		// 		})
		// 		->orWhere(function($query) {
		// 			$query->where('flag_2', '0')
		// 				  ->whereNotNull('flag_1');
		// 		})
		// 		->orWhere(function($query) {
		// 			$query->where('flag_2', '0')
		// 				  ->where('flag_1', '0');
		// 		})
		// 		->inRandomOrder()->limit(2)
		// 		->pluck('id_pegawai');
		// 		do {
		// 		$random1 = json_decode($collection->random(1));
		// 		} while ($random1[0] == $random[0]);
		// 		$user1 = DB::table('pegawai')
		// 		->where('jenis_pegawai', $random1[0])
		// 		->orWhere(function($query) {
		// 			$query->where('flag_1', '0')
		// 				  ->whereNotNull('flag_2');
		// 		})
		// 		->orWhere(function($query) {
		// 			$query->where('flag_2', '0')
		// 				  ->whereNotNull('flag_1');
		// 		})
		// 		->orWhere(function($query) {
		// 			$query->where('flag_2', '0')
		// 				  ->where('flag_1', '0');
		// 		})
		// 		->inRandomOrder()->limit(1)
		// 		->pluck('id_pegawai');
		// 		do {
		// 		$random2 = json_decode($collection->random(1));
		// 		} while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
		// 		$user2 = DB::table('pegawai')
		// 		->where('jenis_pegawai', $random2[0])
		// 		->orWhere(function($query) {
		// 			$query->where('flag_1', '0')
		// 				  ->whereNotNull('flag_2');
		// 		})
		// 		->orWhere(function($query) {
		// 			$query->where('flag_2', '0')
		// 				  ->whereNotNull('flag_1');
		// 		})
		// 		->orWhere(function($query) {
		// 			$query->where('flag_2', '0')
		// 				  ->where('flag_1', '0');
		// 		})
		// 		->inRandomOrder()->limit(1)
		// 		->pluck('id_pegawai');
		// 		$grup = array_merge($user, $user1, $user2);
		// 		$this->pegawaiModel->updatebatch11($grup);
		

		// 				// kelompok 4
		// 				$collection = collect([1, 2, 3]);
		// 				$random = json_decode($collection->random(1));
		// 				$user = DB::table('pegawai')
		// 				->where('jenis_pegawai', $random[0])
		// 				->orWhere(function($query) {
		// 					$query->where('flag_1', '0')
		// 						  ->whereNotNull('flag_2');
		// 				})
		// 				->orWhere(function($query) {
		// 					$query->where('flag_2', '0')
		// 						  ->whereNotNull('flag_1');
		// 				})
		// 				->orWhere(function($query) {
		// 					$query->where('flag_2', '0')
		// 						  ->where('flag_1', '0');
		// 				})
		// 				->inRandomOrder()->limit(2)
		// 				->pluck('id_pegawai');
		// 				do {
		// 				$random1 = json_decode($collection->random(1));
		// 				} while ($random1[0] == $random[0]);
		// 				$user1 = DB::table('pegawai')
		// 				->where('jenis_pegawai', $random1[0])
		// 				->orWhere(function($query) {
		// 					$query->where('flag_1', '0')
		// 						  ->whereNotNull('flag_2');
		// 				})
		// 				->orWhere(function($query) {
		// 					$query->where('flag_2', '0')
		// 						  ->whereNotNull('flag_1');
		// 				})
		// 				->orWhere(function($query) {
		// 					$query->where('flag_2', '0')
		// 						  ->where('flag_1', '0');
		// 				})
		// 				->inRandomOrder()->limit(1)
		// 				->pluck('id_pegawai');
		// 				do {
		// 				$random2 = json_decode($collection->random(1));
		// 				} while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
		// 				$user2 = DB::table('pegawai')
		// 				->where('jenis_pegawai', $random2[0])
		// 				->orWhere(function($query) {
		// 					$query->where('flag_1', '0')
		// 						  ->whereNotNull('flag_2');
		// 				})
		// 				->orWhere(function($query) {
		// 					$query->where('flag_2', '0')
		// 						  ->whereNotNull('flag_1');
		// 				})
		// 				->orWhere(function($query) {
		// 					$query->where('flag_2', '0')
		// 						  ->where('flag_1', '0');
		// 				})
		// 				->inRandomOrder()->limit(1)
		// 				->pluck('id_pegawai');
		// 				$grup = array_merge($user, $user1, $user2);
		// 				$this->pegawaiModel->updatebatch12($grup);
//}