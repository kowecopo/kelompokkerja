<?php

namespace App\Http\Controllers;

use App\Models\pegawaiModel;
use App\Models\viewData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Random extends Controller
{
    //

    public function __construct()
	{
		$this->pegawaiModel = new pegawaiModel();
	}

	public function index()
	{
		foreach($this->pegawaiModel->ReadData() as $row) {
			$kelompok[$row->flag_1 ?? 0][] = $row->nama_pegawai;
			$kelompok[$row->flag_2 ?? 0][] = $row->nama_pegawai;
			$kelompok[$row->flag_3 ?? 0][] = $row->nama_pegawai;
			$kelompok[$row->flag_4 ?? 0][] = $row->nama_pegawai;
		}
		$data = [
			'pegawai' => $this->pegawaiModel->ReadData(),
			'kelompoks' => $kelompok,
		];
		return view('random', $data);
	}

    public function minggu1()
	{
	
		// kelompok 1
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random[0])
		->where('flag_1', 0)->random(2)
		->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random1[0])
		->where('flag_1', 0)->random(1)
		->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] or $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random2[0])
		->where('flag_1', 0)->random(1)
		->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch1($grup);

		// kelompok 2
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->ReadData()
	    ->where('jenis_pegawai', $random[0])
	    ->where('flag_1', 0)->random(2)->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random1[0])
		->where('flag_1', 0)->random(1)->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] or $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random2[0])
		->where('flag_1', 0)->random(1)->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch2($grup);

		// kelompok 3
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->ReadData()
	    ->where('jenis_pegawai', $random[0])
	    ->where('flag_1', 0)->random(2)->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random1[0])
		->where('flag_1', 0)->random(1)->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] or $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random2[0])
		->where('flag_1', 0)->random(1)->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch3($grup);

		// kelompok 4
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->ReadData()
	    ->where('jenis_pegawai', $random[0])
	    ->where('flag_1', 0)->random(2)->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random1[0])
		->where('flag_1', 0)->random(1)->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] or $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random2[0])
		->where('flag_1', 0)->random(1)->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch4($grup);

	}

    public function minggu2()
	{
		// kelompok 1
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random[0])
		->where('flag_2', 0)->random(2)
		->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random1[0])
		->where('flag_2', 0)->random(1)
		->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random2[0])
		->where('flag_2', 0)->random(1)
		->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch5($grup);

		// kelompok 2
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->ReadData()
	    ->where('jenis_pegawai', $random[0])
	    ->where('flag_2', 0)->random(2)->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random1[0])
		->where('flag_2', 0)->random(1)->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random2[0])
		->where('flag_2', 0)->random(1)->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch6($grup);

		// kelompok 3
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->ReadData()
	    ->where('jenis_pegawai', $random[0])
	    ->where('flag_2', 0)->random(2)->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random1[0])
		->where('flag_2', 0)->random(1)->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random2[0])
		->where('flag_2', 0)->random(1)->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch7($grup);

		// kelompok 4
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->ReadData()
	    ->where('jenis_pegawai', $random[0])
	    ->where('flag_2', 0)->random(2)->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random1[0])
		->where('flag_2', 0)->random(1)->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] || $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->ReadData()
		->where('jenis_pegawai', $random2[0])
		->where('flag_2', 0)->random(1)->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch8($grup);

	}

    public function minggu3()
	{


		// kelompok 1
	    $user = json_decode($this->pegawaiModel->Data3()
	    ->where('jenis_pegawai', 2)
	    ->where('flag_3', 0)->random(2)->pluck('id_pegawai'));
	
		$user1 = json_decode($this->pegawaiModel->Data3()
		->where('jenis_pegawai', 1)
		->where('flag_3', 0)->random(1)->pluck('id_pegawai'));
	
		$user2 = json_decode($this->pegawaiModel->Data3()
		->where('jenis_pegawai', 3)
		->where('flag_3', 0)->random(1)->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch9($grup);

			// kelompok 2
			$user = json_decode($this->pegawaiModel->Data3()
			->where('jenis_pegawai', 2)
			->where('flag_3', 0)->random(2)->pluck('id_pegawai'));
		
			$user1 = json_decode($this->pegawaiModel->Data3()
			->where('jenis_pegawai', 1)
			->where('flag_3', 0)->random(1)->pluck('id_pegawai'));
		
			$user2 = json_decode($this->pegawaiModel->Data3()
			->where('jenis_pegawai', 3)
			->where('flag_3', 0)->random(1)->pluck('id_pegawai'));
			$grup = array_merge($user, $user1, $user2);
			$this->pegawaiModel->updatebatch10($grup);

				// kelompok 3
				$user = json_decode($this->pegawaiModel->Data3()
				->where('jenis_pegawai', 2)
				->where('flag_3', 0)->random(2)->pluck('id_pegawai'));
			
				$user1 = json_decode($this->pegawaiModel->Data3()
				->where('jenis_pegawai', 1)
				->where('flag_3', 0)->random(1)->pluck('id_pegawai'));
			
				$user2 = json_decode($this->pegawaiModel->Data3()
				->where('jenis_pegawai', 3)
				->where('flag_3', 0)->random(1)->pluck('id_pegawai'));
				$grup = array_merge($user, $user1, $user2);
				$this->pegawaiModel->updatebatch11($grup);

					// kelompok 4
					$user = json_decode($this->pegawaiModel->Data3()
					->where('jenis_pegawai', 2)
					->where('flag_3', 0)->random(2)->pluck('id_pegawai'));
				
					$user1 = json_decode($this->pegawaiModel->Data3()
					->where('jenis_pegawai', 1)
					->where('flag_3', 0)->random(1)->pluck('id_pegawai'));
				
					$user2 = json_decode($this->pegawaiModel->Data3()
					->where('jenis_pegawai', 3)
					->where('flag_3', 0)->random(1)->pluck('id_pegawai'));
					$grup = array_merge($user, $user1, $user2);
					$this->pegawaiModel->updatebatch12($grup);
    }

    public function minggu4()
	{
	
		// kelompok 1
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->Data4()
		->where('jenis_pegawai', $random[0])
		->where('flag_4', 0)->random(2)
		->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->Data4()
		->where('jenis_pegawai', $random1[0])
		->where('flag_4', 0)->random(1)
		->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] or $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->Data4()
		->where('jenis_pegawai', $random2[0])
		->where('flag_4', 0)->random(1)
		->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch13($grup);

		// kelompok 2
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->Data4()
	    ->where('jenis_pegawai', $random[0])
	    ->where('flag_4', 0)->random(2)->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->Data4()
		->where('jenis_pegawai', $random1[0])
		->where('flag_4', 0)->random(1)->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] or $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->Data4()
		->where('jenis_pegawai', $random2[0])
		->where('flag_4', 0)->random(1)->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch14($grup);

		// kelompok 3
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->Data4()
	    ->where('jenis_pegawai', $random[0])
	    ->where('flag_4', 0)->random(2)->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->Data4()
		->where('jenis_pegawai', $random1[0])
		->where('flag_4', 0)->random(1)->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] or $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->Data4()
		->where('jenis_pegawai', $random2[0])
		->where('flag_4', 0)->random(1)->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch15($grup);

		// kelompok 4
        $collection = collect([1, 2, 3]);
		$random = json_decode($collection->random(1));
	    $user = json_decode($this->pegawaiModel->Data4()
	    ->where('jenis_pegawai', $random[0])
	    ->where('flag_4', 0)->random(2)->pluck('id_pegawai'));
		do {
		$random1 = json_decode($collection->random(1));
		} while ($random1[0] == $random[0]);
		$user1 = json_decode($this->pegawaiModel->Data4()
		->where('jenis_pegawai', $random1[0])
		->where('flag_4', 0)->random(1)->pluck('id_pegawai'));
		do {
		$random2 = json_decode($collection->random(1));
		} while ($random2[0] == $random1[0] or $random2[0] == $random[0]);
		$user2 = json_decode($this->pegawaiModel->Data4()
		->where('jenis_pegawai', $random2[0])
		->where('flag_4', 0)->random(1)->pluck('id_pegawai'));
		$grup = array_merge($user, $user1, $user2);
		$this->pegawaiModel->updatebatch16($grup);

	}

    public function random_pegawai()
    {
    while(true){

		try{
			DB::table('pegawai')->update(array('flag_1' => 0, 'flag_2' => 0, 'flag_3' => 0, 'flag_4' => 0));
            $this->minggu1();
            $this->minggu2();
            $this->minggu3();
            $this->minggu4();

			foreach($this->pegawaiModel->ReadData() as $row) {
				$kelompok[$row->flag_1][] = $row->nama_pegawai;
				$kelompok[$row->flag_2][] = $row->nama_pegawai;
				$kelompok[$row->flag_3][] = $row->nama_pegawai;
				$kelompok[$row->flag_4][] = $row->nama_pegawai;
			}

			
				$data = [
					'pegawai' => $this->pegawaiModel->ReadData(),
					'kelompoks' => $kelompok,
				];
				return view('random', $data);

		} catch (\InvalidArgumentException | \ErrorException $exception ){

		}
	
			

				}
         
    }

	public function simpan(){

		$this->pegawaiModel->simpan_lain();
		foreach($this->pegawaiModel->ReadData() as $row) {
			$kelompok[$row->flag_1 ?? 0][] = $row->nama_pegawai;
			$kelompok[$row->flag_2 ?? 0][] = $row->nama_pegawai;
			$kelompok[$row->flag_3 ?? 0][] = $row->nama_pegawai;
			$kelompok[$row->flag_4 ?? 0][] = $row->nama_pegawai;
		}
		$data = [
			'pegawai' => $this->pegawaiModel->ReadData(),
			'kelompoks' => $kelompok,
		];
		return view('random', $data);

	}

	public function truncate(){
		
		$this->pegawaiModel->truncate();
		foreach($this->pegawaiModel->ReadData() as $row) {
			$kelompok[$row->flag_1 ?? 0][] = $row->nama_pegawai;
			$kelompok[$row->flag_2 ?? 0][] = $row->nama_pegawai;
			$kelompok[$row->flag_3 ?? 0][] = $row->nama_pegawai;
			$kelompok[$row->flag_4 ?? 0][] = $row->nama_pegawai;
		}
		$data = [
			'pegawai' => $this->pegawaiModel->ReadData(),
			'kelompoks' => $kelompok,
		];
		return view('random', $data);

	}


	public function edit($id){

		$data = [
			'pegawai' => $this->pegawaiModel->detailedit($id),
		];
		return view('update_form', $data);

	}

	public function update_random($id)
	{

		$pegawai = [
			'nama_pegawai' => Request()->nama_pegawai,
			'flag_1' => Request()->flag_1,
			'flag_2' => Request()->flag_2,
			'flag_3' => Request()->flag_3,
			'flag_4' => Request()->flag_4,
		];

		$this->pegawaiModel->update_random($id, $pegawai);
		return redirect()->route('random')->with('success', 'Data berhasil diganti');
	}

	public function show_data(Request $request)
	{

		
		foreach(viewData::whereMonth('created_at', $request->id)->get() as $row) {
			$kelompok[$row->flag_1][] = $row->nama_pegawai;
			$kelompok[$row->flag_2][] = $row->nama_pegawai;
			$kelompok[$row->flag_3][] = $row->nama_pegawai;
			$kelompok[$row->flag_4][] = $row->nama_pegawai;
		}

		//dd($kelompok);exit;
		return response()->json([
			'code'=> 1,
			'kelompoks' => $kelompok,
		]);
	}
    
}