<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pegawaiModel;


class Dashboard extends Controller
{
    public function __construct()
	{
		$this->pegawaiModel = new pegawaiModel();
	}

	public function index(){
		$kelompok = null ;
		foreach($this->pegawaiModel->ReadDataBaru() as $row) {
			$kelompok[$row->flag_1][] = $row->nama_pegawai;
			$kelompok[$row->flag_2][] = $row->nama_pegawai;
			$kelompok[$row->flag_3][] = $row->nama_pegawai;
			$kelompok[$row->flag_4][] = $row->nama_pegawai;
		}
		$data = [
			'total' => $this->pegawaiModel->countDatatotal(),
			'pegawai' => $this->pegawaiModel->countDataPegawai(),
            'penguji' => $this->pegawaiModel->countDataPenguji(),
			'pegawais' => $this->pegawaiModel->ReadDataBaru(),
			'pegawais_1' => $this->pegawaiModel->Flag1(),
			'pegawais_2' => $this->pegawaiModel->Flag2(),
			'pegawais_3' => $this->pegawaiModel->Flag3(),
			'pegawais_4' => $this->pegawaiModel->Flag4(),
			'kelompoks' => $kelompok,
		];
		return view('dashboard', $data);
	}
}
