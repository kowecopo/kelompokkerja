<?php

use App\Http\Controllers\Dashboard;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Pegawai;
use App\Http\Controllers\Random;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', [Dashboard::class, 'index'])->name('dashboard');

Route::get('/pegawai', [Pegawai::class, 'index'])->name('pegawai');
Route::get('/pegawai_view', [Pegawai::class, 'view'])->name('view_data');
Route::get('/pegawai_create', [Pegawai::class, 'view_create'])->name('view_create');
Route::post('/pegawai_create_lesgo', [Pegawai::class, 'create_pegawai'])->name('create_pegawai');
Route::get('/pegawai_edit/{id}', [Pegawai::class, 'view_edit'])->name('view_edit');
Route::post('/pegawai_edit_lesgo/{id}', [Pegawai::class, 'update_pegawai'])->name('update');
Route::get('/pegawai_delete/{id}', [Pegawai::class, 'deleteData'])->name('delete_pegawai');




Route::get('/random', [Random::class, 'index'])->name('random');
Route::get('/random_pegawai', [Random::class, 'random_pegawai'])->name('random_pegawai');
Route::get('/random_simpan', [Random::class, 'simpan'])->name('simpan_pegawai');
Route::get('/random_edit/{id}', [Random::class, 'edit'])->name('edit_random');
Route::post('/random_update/{id}', [Random::class, 'update_random'])->name('update_random');
Route::get('/view_data', [Random::class, 'show_data'])->name('show_data');


Route::get('/truncate_view', [Random::class, 'truncate'])->name('truncate_view');
